package task03;

/** Creator
* (������ ������������
* Factory Method)<br>
* ������� �����,
* "����������" ��'����
* @author Kot_Shredingera
* @version 1.0beta
* @see Viewable#getView()
*/
public interface Viewable {

/** ������� ��'���, ���� ������ {@linkplain View} */
	public View getView();

}