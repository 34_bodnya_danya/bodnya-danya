package task03;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import task03.Item;
/** ConcreteProduct
* (������ ������������
* Factory Method)<br>
* ���������� �������,
* ���������� � �����������
* ����������
* @author bodnya_danya
* @version 1.0beta
* @see View
*/
public class ViewResult implements View {

/** ��� �����, ���� ��������������� ��� ���������� */
	private static final String FNAME = "items.txt";

/** ��������� ������� ������� ��� ���������� �� ������������� */
	private static final int DEFAULT_NUM = 10;

/** ArrayList ��������� �� ���������� ��������� */
	private ArrayList<Item> items = new ArrayList<Item>();

/** ������� {@linkplain ViewResult#ViewResult(int n) ViewResult(int n)}
* � ���������� {@linkplain ViewResult#DEFAULT_NUM DEFAULT_NUM}
*/
	public ViewResult() {
		this(DEFAULT_NUM);
	}

/** ������������ ArrayList {@linkplain ViewResult#items}
* @param n ��������� ������� ��������
*/
	public ViewResult(int n) {
		for(int ctr = 0; ctr < n; ctr++) {
			items.add(new Item());
		}
	}

/** �������� �������� {@linkplain ViewResult#items}
* @return ������� �������� ��������� �� ��'��� {@linkplain ArrayList}
*/
	public ArrayList<Item> getItems() {
		return items;
	}

/** �������� �������� �������
* @param triangle - ������� ������� ��������������� ����������
* @param square - ������� ������� ��������������� ������������
* @return ��������� ���������� ������
*/
	private double calc(int triangle, int square) {
		return (square*4) +(Math.pow(triangle, 2)*Math.sqrt(3)/4);
	}

/** �������� �������� ������� �� ������� ��
* ��������� � ArrayList {@linkplain ViewResult#items}
* @param stepTriangle ���� ��������� ���������
* @param stepSquare ���� ��������� ���������
*/
	public void init(int stepTriangle, int stepSquare) {
		int triangle = 0, square = 0;
		
		for(Item item : items) {
			item.setTriangleSquareArea(triangle, square, calc(triangle, square));
			triangle += stepTriangle;
			square +=stepSquare;
		}
	}

/** ������� <b>init(double stepX)</b> � �������� �������� ��������� ���������<br>
* {@inheritDoc}
*/
	@Override
	public void viewInit() {
		String stepTriangle = Integer.toBinaryString((int)(Math.random()*180));
		String stepSquare = Integer.toBinaryString((int)(Math.random()*180));//��������� ����� � ��������� ���
		init(Integer.parseInt(stepTriangle, 2),  Integer. parseInt(stepSquare, 2));//������� �������� ��������� ��� ������������ � ����� � ��������� ������
	}

/**��������� ������ {@linkplain View#viewSave()}<br>
* {@inheritDoc}
*/
	@Override
	public void viewSave() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
		os.writeObject(items);
		os.flush();
		os.close();
	}

/** ���������� ������ {@linkplain View#viewRestore()}<br>
* {@inheritDoc}
*/
	@SuppressWarnings("unchecked")
	@Override
	public void viewRestore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		items = (ArrayList<Item>) is.readObject();
		is.close();
	}

/** ���������� ������ {@linkplain View#viewHeader()}<br>
* {@inheritDoc}
*/
	@Override
	public void viewHeader() {
		System.out.println("Results:");
	}

/** ���������� ������ {@linkplain View#viewBody()}<br>
* {@inheritDoc}
*/
	@Override
	public void viewBody() {
		for(Item item : items) {
			System.out.printf("(%d; %d; %.2f) \n", item.getTriangle(), item.getSquare(), item.getArea());
		}
		System.out.println();
	}

/** ���������� ������ {@linkplain View#viewFooter()}<br>
* {@inheritDoc}
*/
	@Override
	public void viewFooter() {
		System.out.println("End.");
	}

/** ���������� ������ {@linkplain View#viewShow()}<br>
* {@inheritDoc}
*/
	@Override
	public void viewShow() {
		viewHeader();
		viewBody();
		viewFooter();
	}
}
