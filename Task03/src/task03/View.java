package task03;

import java.io.IOException;

/** Product
* (������ ������������
* Factory Method)<br>
* ��������� "����������"
* ��'����<br>
* ������� ������
* ³������� ��'����
* @author Kot_Shredingera
* @version 1.0beta
*/
public interface View {
/** ³���������� ��������� */
	public void viewHeader();

/** ³���������� ������� ������� */
	public void viewBody();

/** ³������� ��������� */
	public void viewFooter();

/** ³������� ��'��� �������� */
	public void viewShow();

/** ��������� ������������ */
	public void viewInit();

/** ������ ����� ��� ���������� ���������� */
	public void viewSave() throws IOException;

/** ³������� ������ ��������� ����� */
	public void viewRestore() throws Exception;
}
