package task03;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
/** ���������� �� ��������� ����������.
* �� ��������� ���������� ������ main().
* @author Kot_Shredingera
* @version 1.0beta
* @see Main#main
*/
public class Main {
	/** ��'��� ����� {@linkplain Calculat}. */
	private Calculat calc = new Calculat();
	
	/** ��'���, ���� ������ ��������� {@linkplain View};
	* ��������� ������ ��'���� {@linkplain task03.Item}
	*/
	private View view;
	
	/** ��������� ���� {@linkplain Main#view view}. */
	public Main(View view) {
	this.view = view;
	}
	
	/** ³���������� ����. */
	private void menu() {
		String s = null;
	
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			
		do {
			do {
				System.out.println("������ �������");
				System.out.print("1-�����, \n"
						+ "2-�����������, \n"
						+ "3-�����������, \n"
						+ "4-��������, \n"
						+ "5-��������: \n");
				try {
					s = in.readLine();
				} catch(IOException e) {
					System.out.println("Error: " + e);
					System.exit(0);
				}
			} while (s.length() != 1);
			switch (s.charAt(0)) {
			
			case '1': // ����� � ��������
				System.out.println("�����.");
				break;
			
			case '2': // �������� �������� �����
				System.out.println("����������� ��������.");
				view.viewShow();
				break;
			
			case '3': // ��������� �����
	
				try {
				view.viewInit();
				view.viewShow();
				}catch(Exception e) {
					System.out.println("������� : " + e);
				}
				break;
			
			case '4': //���������� �������� ������
				System.out.println("�������� �������.");
				try {
					view.viewSave();
				} catch (IOException e) {
					System.out.println("������� ����������: " + e);
				}
				view.viewShow();
				break;
			
			case '5': //�������� ������� ����� � �����
				System.out.println("³������� ������� ��������� �����.");
				try {
					view.viewRestore();
				} catch (Exception e) {
					System.out.println("������� ������������: " + e);
				}
				view.viewShow();
				break;
			default:
				System.out.print("������ ������� ");
			}
			
		} while(s.charAt(0) != '1');
	}
	
/** ���������� ��� ��������� ��������.
* ���������� ��������� ��� ����� ��������� �� �����.
* @param args - �������� ������� ��������.
*/
	public static void main(String[] args) {
		Main main = new Main(new ViewableResult().getView());
		main.menu();
		}
}
