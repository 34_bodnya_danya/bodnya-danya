package task03;

/** ConcreteCreator
* (������ ������������
* Factory Method)<br>
* ��'����� ������,
* "����������" ��'����
* @author bodnya_danya
* @version 1.0beta
* @see Viewable
* @see ViewableResult#getView()
*/
public class ViewableResult implements Viewable {

/** ������� ��������� ��'��� {@linkplain ViewResult} */
	@Override
	public View getView() {
		return new ViewResult();
	}
	
}