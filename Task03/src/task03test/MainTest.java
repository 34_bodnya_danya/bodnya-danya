package task03test;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import junit.framework.Assert;
import java.io.IOException;
import task03.Calculat;
import task03.Item;
import task03.ViewResult;
/** ������ ���������� ����������� ������.
* @author Kot_Shredingera
* @version 1.0beta
*/

public class MainTest {
	/** �������� ������� ��������������� ����� {@linkplain Calculat} */
	@Test
	public void testCalc() {
		ViewResult view = new ViewResult(5);
		view.init(90, 90);
		Item item = new Item();
		int ctr = 0;
		item.setTriangleSquareArea(0, 0, 0);
		assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
		view.getItems().get(ctr).equals(item));
		ctr++;
		item.setTriangleSquareArea(0, 1, 4);
		assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
		view.getItems().get(ctr).equals(item));
		ctr++;
		
	
	}
	
	/** �������� ����������. ����������� ���������� �����. */
	@Test
	public void testRestore() {
		ViewResult view1 = new ViewResult(1000);
		ViewResult view2 = new ViewResult();
		
		// ��������� �������� ������� � ���������� ������ ���������� ���������
		view1.init((int)(Math.random()*100), (int)(Math.random()*100));
		// �������� ������ view1.items
		try {
		view1.viewSave();
		} catch (IOException e) {
		Assert.fail(e.getMessage());
		}
		
		// ����������� ��������� view2.items
		try {
		view2.viewRestore();
		} catch (Exception e) {
		Assert.fail(e.getMessage());
		}
		// ������� ������������� ������ ��������, ������ ���� ����������� 
		assertEquals(view1.getItems().size(), view2.getItems().size());
		// �� �������� ������� ���� ������.
		// ��� ����� ��������� ��������� equals
		assertTrue("containsAll()", view1.getItems().containsAll(view2.getItems()));
		
		
		
		
	
			
		}
}


