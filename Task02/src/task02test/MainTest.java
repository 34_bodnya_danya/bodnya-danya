package task02test;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import junit.framework.Assert;
import java.io.IOException;
import task02.Calculat;
/** ������ ���������� ����������� ������.
* @author Kot_Shredingera
* @version 1.0beta
*/

public class MainTest {
	/** �������� ������� ��������������� ����� {@linkplain Calculat} */
	@Test
	public void testCalc() {
		Calculat calc = new Calculat();
		
		calc.init(0 , 0);
		assertEquals(0,0);
		
		calc.init(1011, 1011);
		assertEquals(1,  1);

		calc.init(11, 001);
		assertEquals(1,1);
	
	}
	
	/** �������� ����������. ����������� ���������� �����. */
	@Test
	public void testRestore() {
		Calculat calc = new Calculat();
		String triangle, square;
		double area;
		for(int ctr = 0; ctr < 1000; ctr++) {
			triangle =Integer.toBinaryString((int)(Math.random()*180));
			square = Integer.toBinaryString((int)(Math.random()*180));//�������� ����� � ��������� ���
			area = calc.init( Integer.parseInt(triangle),  Integer. parseInt(square)); 
			try {
				calc.save();
			} catch (IOException e) {
				Assert.fail(e.getMessage());
			}
			calc.init(Integer.parseInt(triangle),  Integer. parseInt(square));
			try {
				calc.restore();
			} catch (Exception e) {
				Assert.fail(e.getMessage());
			}
			
		}
	}
}
