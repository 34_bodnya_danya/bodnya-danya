package task02;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
/** ���������� �� ��������� ����������.
* �� ��������� ���������� ������ main().
* @author Kot_Shredingera
* @version 1.0beta
* @see Main#main
*/
public class Main {
	/** ��'��� ����� {@linkplain Calculat}. */
	private Calculat calc = new Calculat();
	
	/** ³���������� ����. */
	private void menu() {
		String s = null;
		int triangle = 0;
		int square = 0;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			
		do {
			do {
				System.out.println("������ �������");
				System.out.print("1-�����, \n"
						+ "2-�����������, \n"
						+ "3-������� �����, \n"
						+ "4-��������, \n"
						+ "5-��������: \n");
				try {
					s = in.readLine();
				} catch(IOException e) {
					System.out.println("Error: " + e);
					System.exit(0);
				}
			} while (s.length() != 1);
			switch (s.charAt(0)) {
			
			case '1': // ����� � ��������
				System.out.println("�����.");
				break;
			
			case '2': // �������� �������� �����
				System.out.println("����������� ��������.");
				calc.show();
				break;
			
			case '3': // �������� �����
				
				System.out.println("������ ����� ����� � ������� ������");
				try {
					triangle = Integer.parseInt(in.readLine());
				} catch (Exception e1) {
					System.out.println("������� : " + e1);
				} //�������� ������� �����
				
				System.out.println("������ ����� ����� � ������� ������");
				try {
					square = Integer.parseInt(in.readLine());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					System.out.println("������� : " + e1);
				} //�������� ������� �����
				
				try {
				calc.init(triangle, square);
				calc.show();
				}catch(Exception e) {
					System.out.println("������� : " + e);
				}
				break;
			
			case '4': //���������� �������� ������
				System.out.println("�������� �������.");
				try {
					calc.save();
				} catch (IOException e) {
					System.out.println("������� ����������: " + e);
				}
				calc.show();
				break;
			
			case '5': //�������� ������� ����� � �����
				System.out.println("³������� ������� ��������� �����.");
				try {
					calc.restore();
				} catch (Exception e) {
					System.out.println("������� ������������: " + e);
				}
				calc.show();
				break;
			default:
				System.out.print("������ ������� ");
			}
			
		} while(s.charAt(0) != '1');
	}
	
/** ���������� ��� ��������� ��������.
* ���������� ��������� ��� ����� ��������� �� �����.
* @param args - �������� ������� ��������.
*/
	public static void main(String[] args) {
		Main main = new Main();
		main.menu();
		}
}
