package task02;

import java.io.Serializable;
/** ������ �������� ������ �� ���������� ����������
* @author Kot_Shredingera
* @version 1.0beta
*/
public class Item implements Serializable {

/** ������� ������� ��������������� ���������� */
	// transient
	private int triangle;
	
/** ������� ������� ��������������� ������������*/
	private int square;
	
/** ���� ���� ��������������� ���������� �� ��������������� ������������*/
	private double area;

/** ����������� �������� ��������� */
	private static final long serialVersionUID = 1L;

/** ������������ ���� {@linkplain Item#triangle}, {@linkplain Item#square}, {@linkplain Item#area} */
	public Item() {
		triangle = 0;
		square= 0;
		area = .0;
	}

/** ���������� �������� ����: ������
* �� ���������� ���������� �����.
* @param triangle - �������� ��� ������������ ���� {@linkplain Item#triangle}
* @param square - �������� ��� ������������ ���� {@linkplain Item#square}
* @param area - �������� ��� ������������ ���� {@linkplain Item#area}
*/
	public Item(int triangle, int square, double area) {
		this.triangle = triangle;
		this.square = square;
		this.area = area;
	}

/**������������ ������� ���� {@linkplain Item#triangle}
* @param triangle - �������� ��� {@linkplain Item#triangle}
* @return �������� {@linkplain Item#triangle}
*/
	public int setTriangle(int triangle) {
		return this.triangle = triangle;
	}

/** ��������� �������� ���� {@linkplain Item#triangle}
* @return �������� {@linkplain Item#triangle}
*/
	public int getTriangle() {
		return triangle;
	}

/** ������������ �������� ���� {@linkplain Item#square}
* @param square - �������� ��� {@linkplain Item#square}
* @return �������� {@linkplain Item#square}
*/
	public int setSquare(int square) {
		return this.square = square;
	}

/** ��������� �������� ���� {@linkplain Item#square}
* @return �������� {@linkplain Item#square}
*/
	public int getSquare() {
		return square;
	}

/** ������������ �������� ���� {@linkplain Item#area}
* @param area - �������� ��� {@linkplain Item#area}
* @return �������� {@linkplain Item#area}
*/
	public double setArea(double area) {
		return this.area = area;
	}

/** ��������� �������� ���� {@linkplain Item#area}
* @return �������� {@linkplain Item#area}
*/
	public double getArea() {
		return area;
	}	

/** ������������ ������� {@linkplain Item#triangle}, {@linkplain Item#square} � {@linkplain Item#area}
* @param triangle - �������� ��� {@linkplain Item#triangle}
* @param square - �������� ��� {@linkplain Item#square}
* @param area - �������� ��� {@linkplain Item#area}
* @return this
*/
	public Item setTriangleSquareArea(int triangle, int square, double area) {
		this.triangle = triangle;
		this.square = square;
		this.area = area;
		return this;
	}

/** ������������ ��������� ���������� � ���� ������.<br>{@inheritDoc} */
	@Override
	public String toString() {
		return "triangle = " + triangle + ", square = " + square + ", area=" +area;
	}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	long temp;
	temp = Double.doubleToLongBits(area);
	result = prime * result + (int) (temp ^ (temp >>> 32));
	result = prime * result + square;
	result = prime * result + triangle;
	return result;
}

/** ����������� ��������� �����.<br>{@inheritDoc} */
@Override
public boolean equals(Object obj) {
	if (this == obj) {
		return true;
	}
	if (obj == null) {
		return false;
	}
	if (getClass() != obj.getClass()) {
		return false;
	}
	Item other = (Item) obj;
	if (Double.doubleToLongBits(area) != Double.doubleToLongBits(other.area)) {
		return false;
	}
	if (square != other.square) {
		return false;
	}
	if (triangle != other.triangle) {
		return false;
	}
	return true;
}

}
