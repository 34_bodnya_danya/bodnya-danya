package task02;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
/** ���������� �� ��������� ����������
* @author Kot_Shredingera
* @version 1.0beta
*/

public class Calculat {

/** ��� ����� ��� ��������������� ��� �����������. */
	private static final String FNAME = "Item.txt";

/** ������ ���������� ���������. ��'��� ����� {@linkplain Item} */
	private Item result;

/** ��������� ���� {@linkplain Calculat#result} */
	public Calculat() {
		result = new Item();
	}

/** ���������� �������� {@linkplain Calculat#result}
* @param result - ���� �������� ��������� �� ��'��� {@linkplain Item}
*/
	public void setResult(Item result) {
		this.result = result;
	}	

/** �������� �������� {@linkplain Calculat#result}
* @return ������� �������� ��������� �� ��'���{@linkplain Item}
*/
	public Item getResult() {
		return result;
	}

/** ���������� �������� �������.
* @param triangle - ��������, ������� ��������������� ����������.
* @param square - ��������, ������� ��������������� ������������.
* @return ��������� ���������� �������.
*/
	private double calc(int triangle,int square ) {
		return (square*4) +(Math.pow(triangle, 2)*Math.sqrt(3)/4);
	}

/** �������� �������� ������� �� �������
* ��������� � ��'���{@linkplain Calculat#result}
* @param triangle - ��������, ������� ��������������� ����������.
* @param square - ��������, ������� ��������������� ������������.
*/
	public double init(int triangle, int square) {
		triangle = Integer.parseInt(""+triangle, 2);
		square = Integer.parseInt(""+square, 2);
		result.setTriangle(triangle);
		result.setSquare(square);
		return result.setArea(calc(triangle, square));
	}

/** ��������� ���������� ���������. */
	public void show() {
		System.out.println(result);
	}

/** ������ {@linkplain Calculat#result} � ���� {@linkplain Calculat#FNAME}
* @throws IOException
*/
	public void save() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new
				FileOutputStream(FNAME));
		os.writeObject(result);
		os.flush();
		os.close();
	}	

/** ���������� {@linkplain Calculat#result} � ����� {@linkplain Calculat#FNAME}
* @throws Exception
*/
	public void restore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		result = (Item)is.readObject();
		is.close();
	}
}